x3LE Synthesizable Soft Processor
==================================

Features
---------
- 16 bit word size
- 64 registers
	- 56 General Purpose
	- 8 'Internal ones'
- 2048 program words
- up to 64kB data stack
- 16 level deep call stack
- Simple architecture design
- Whacked up in a weekendworth of time more or less as a test
- It somehow works!
- Its IO sucks so far, working on it.

Description
------------
The x3LE soft core is based upon my old x3L design, originally made to be a super minimalistic architecture designed to be turned into a Minecraft computer, and it actually ended up getting implemented over a weekend. The x3LE further extends and modifies the x3L architecture, taking a feature or two from it, however fairly modified.

One of those features is the fact that the ALU is treated as being a mostly separate module. There are no direct arithmetic instructions. You can only use the ALU by loading data into the input registers, and then loading your ALU mode into its mode register. The output register is read-only, and changes whenever one loads in a new mode or new data, as expected. I've chosen this approach originally to simplify the instruction set, however there are still quite some performance drawbacks to it.

One may even say writing code for this arch kind of feels like writing microcode!

The new features include a direct stack support (up to 64kB in size (wow!), but that can be probably extended), a sh*tload of GPRs right in the processor core, indirect addressing over the said registers (not fully implemented in the current version) and that's about it, really.

I have yet to write a full-on documentation of this thing, but most of the actual stuff can be gathered by looking at the spec text file within the `doc` directory somewhere.

Some further description is to be found within the repo's `doc` directory. The source code should be inside the `src` directory, as expected.

Todo:
------
- Add interrupt support
- Make a way better documentation
- Fix this README as well.
- Rework the register file a bit, to allow for using some register-mapped peripherals
- Add said peripherals
	- Timer/counters
	- PWM
	- UART
	- Generic IO ports
- Create some assembler/disassembler package for it, assembling it manually is a bit of a pain
- Optimize timing a fair bit, consider pipelining, do the appropriate changes to support that.
