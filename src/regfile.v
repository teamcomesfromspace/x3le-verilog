`timescale 1ns / 1ps

module x3le_regfile(
    input             reset,

    input      [ 7:0] wdat,
    input      [ 5:0] wadr, //
    input             wclk, //write clock
    input             wen,  //write enable - enables writing to regs
                            //if it's zero, stack pointer can still be incremented/decremented
    output     [ 7:0] rdat,
    input      [ 5:0] radr,
    
    output            flg_carry,
    output            flg_zero,
    
    output     [15:0] spo,
    
    input             sp_up,
    input             sp_down
  );

  
  `define R_INA (6'b000000)
  `define R_INB (6'b000001)
  `define R_FCN (6'b000010)
  `define R_OUT (6'b000011)
  `define R_SPL (6'b000100)
  `define R_SPH (6'b000101)
  `define R_INX (6'b000110)
  `define R_INR (6'b000111)
  
  /// Stack counter ////////////////////////////////////////////////////////////

  reg  [15:0] sp;
  
  assign spo = sp;
  
  
  /// Register file itself /////////////////////////////////////////////////////
  
  reg  [ 7:0] regfile [63:8];
  
  /// 8 lower registers

  reg  [ 7:0] alu_ina;
  reg  [ 7:0] alu_inb;
  reg  [ 7:0] alu_mode;
  wire [ 7:0] alu_out;  
  
  reg  [ 5:0] inx;  //index for indirect addressing
  
  /// Register Writing /////////////////////////////////////////////////////////
  
  always @(posedge wclk or posedge reset) begin
    if(reset) begin
      sp = -1;
      inx = 6'b001000;
    end
    else
    begin
    //Actual data writing
      if(wen) begin
        $display("Reg write (%d)[%b]", wdat, wadr);
        case(wadr)
          `R_INA: alu_ina  = wdat;
          `R_INB: alu_inb  = wdat;
          `R_FCN: alu_mode = wdat;
          `R_OUT: begin end
          `R_SPL: sp = {sp[15:8], wdat};
          `R_SPH: sp = {wdat, sp[7:0]};
          `R_INX: inx = wdat;
          `R_INR: regfile[inx] = wdat;
          default:
            regfile[wadr] = wdat;
        endcase
      end
      
      if ( sp_up & !sp_down)
        sp = sp + 1;
      else
        if (!sp_up & sp_down)
            sp = sp - 1;
    end    
  end
  
  /// Register Reading /////////////////////////////////////////////////////////
  
  
  wire [7:0] lowregs [7:0];
  assign lowregs[`R_INA] = alu_ina[7:0];
  assign lowregs[`R_INB] = alu_inb[7:0];
  assign lowregs[`R_FCN] = alu_mode[7:0];
  assign lowregs[`R_OUT] = alu_out[7:0];
  assign lowregs[`R_SPL] = sp[ 7:0];
  assign lowregs[`R_SPH] = sp[15:8];
  assign lowregs[`R_INX] = inx;
  assign lowregs[`R_INR] = regfile[inx];

  assign rdat = (3'b000 == radr[5:3]) ? lowregs[radr] : regfile[radr];
  
  /// ALU Connection ///////////////////////////////////////////////////////////

  // For those wondering why on earth would there be an ALU
  //  inside a register regfile:
  // ------------------------------------------------------
  // The x3LE architecture builds on some early ideas
  //  meant to simplify actual decoder design and stuff,
  //  mapping all the ALU stuff straight into register space.
  // I've decided to reuse this concept in x3LE, again
  //  to make program decoding about as simple as it gets.
  //  The actual ALU operation decoding from the 8bit word
  //  would be done in the actual ALU, probably with a simple
  //  Look-Up table.
  //
  
  alu #(.WIDTH(8)) alu0 (
    .ina      (  alu_ina  ),
    .inb      (  alu_inb  ),
    .cin      (   1'b0    ),
    .fcn      ( alu_mode  ),
    .cout     ( flg_carry ),
    .flgzero  ( flg_zero  ),
    .out      (  alu_out  )
  );
  
endmodule
