`timescale 1ns / 1ps

  module x3le(
      input clk,
      input reset,
      
      input  [15:0] p_dat,
      output [11:0] p_adr,
      output reg    p_rd,
      
      output     [15:0] d_adr,
      input      [ 7:0] d_in,
      output reg [ 7:0] d_out,
      output reg        d_wr,
      output reg        d_rd
  );
  
  /// Register File instantiation //////////////////////////////////////////////

  wire [ 7:0] r_wdat;
  wire [ 7:0] r_rdat;
  
  wire        r_flg_carry;
  wire        r_flg_zero;
   
  wire [ 5:0] r_wadr;
  wire [ 5:0] r_radr;
  
  wire [15:0] r_sp;
  
  reg         r_wclk;
  reg         r_wen;
  
  reg         r_spup;
  reg         r_spdn;
  
  x3le_regfile regfile (
    .reset     (reset), 
    .wdat      (r_wdat), 
    .wen       (r_wen),
    .wadr      (r_wadr), 
    .wclk      (r_wclk), 
    .rdat      (r_rdat), 
    .radr      (r_radr),
    .flg_carry (r_flg_carry),
    .flg_zero  (r_flg_zero),
    .spo       (r_sp),
    .sp_up     (r_spup),
    .sp_down   (r_spdn)
  );
  
  
  /// Program counter //////////////////////////////////////////////////////////
  
  reg    [11:0] pc;
  assign        p_adr =   pc;
  assign        d_adr = r_sp;
  
  /// Random wire/regs /////////////////////////////////////////////////////////
  
  reg    [15:0] p_lat;                 //program latch                    
  
  wire   [ 7:0] lddat;
  assign        lddat  = p_lat [13:6]; //data bus used in the LD instruction
  
  assign        r_wadr = p_lat [ 5:0]; //read  address bus
  assign        r_radr = p_lat [11:6]; //write address bus
  
  wire   [11:0] jpadr;
  assign        jpadr  = p_lat [11:0]; //jump address
  
  /// Register write input mux /////////////////////////////////////////////////
  
  `define WS_RDAT (2'b00)
  `define WS_LDAT (2'b01)
  `define WS_IOIN (2'b10)
  `define WS_ZERO (2'b11)
  
  reg    [ 1:0] r_wsel;
  assign r_wdat = (r_wsel == `WS_RDAT) ? r_rdat     : {8{1'bZ}};
  assign r_wdat = (r_wsel == `WS_LDAT) ? lddat      : {8{1'bZ}};
  assign r_wdat = (r_wsel == `WS_IOIN) ? d_in       : {8{1'bZ}};
  assign r_wdat = (r_wsel == `WS_ZERO) ? {8{1'b0}}  : {8{1'bZ}};
  
  /// Internal jump stack //////////////////////////////////////////////////////
  
  reg [11:0] jstack [15:0];
  reg [ 3:0] jsp;
  
  /// Actual controller logic //////////////////////////////////////////////////
  
  reg    [ 1:0] phase;    //0 = prog latch
                          //1 = reg read + proc
                          //2 = ?
                          //3 = writeback
  
  `define INSTR_LOAD (16'b00XX_XXX_XXX_XXX_XXX)
  `define INSTR_MOVE (16'b1000_XXX_XXX_XXX_XXX)
  `define INSTR_RET  (16'b1001_XXX_XXX_XXX_XXX)
  `define INSTR_PUSH (16'b1100_XXX_XXX_XXX_XXX)
  `define INSTR_POP  (16'b1101_XXX_XXX_XXX_XXX)
  `define INSTR_JIZ  (16'b1010_XXX_XXX_XXX_XXX)
  `define INSTR_JIC  (16'b1011_XXX_XXX_XXX_XXX)
  `define INSTR_JMP  (16'b1110_XXX_XXX_XXX_XXX)
  `define INSTR_CALL (16'b1111_XXX_XXX_XXX_XXX)
  
  always @(posedge clk or posedge reset) begin
    if(reset) begin
      phase  = 0;
      pc     = 0;
      jsp    = 0;
    end else begin
      case(phase)
        0:  //Phase 0: reset everything to defaults, assert p_rd. (progmem read clk)
          begin
            d_wr     = 0;  //IO write clk
            d_rd     = 0;  //IO read  clk
            p_rd     = 1;  //Prog read
            r_wclk   = 0;  //Reg. write clk
            r_wen    = 1;
            r_spup   = 0;
            r_spdn   = 0;
          end
        1:  //Phase 1: exec stage 1
          begin
            p_lat = p_dat;
            
            $display("\n\nP: %b", p_lat);
            
            casex(p_lat)
              `INSTR_LOAD:
                begin
                  $display ("LD");
                  r_wsel = `WS_LDAT;
                end
              
              `INSTR_MOVE:
                begin
                  $display ("MV");
                  r_wsel = `WS_RDAT;
                end
              
              `INSTR_RET:
                begin
                  $display ("RET");
                    //deliberatery empty
                end
              
              `INSTR_PUSH:
                begin
                  $display ("PUSH");
                  r_wen = 0;
                  r_spup = 1;
                end
                
              `INSTR_POP:
                begin
                  $display ("POP");
                  d_rd = 1;
                  r_wsel = `WS_IOIN;
                  r_spdn = 1;
                end
                
              `INSTR_JIC:
                begin
                  $display ("JIC");
                    //deliberatery empty
                end
                
              `INSTR_JIZ:
                begin
                  $display ("JIZ");
                    //deliberatery empty
                end
                
              `INSTR_JMP:
                begin
                  $display ("JMP");
                    //deliberatery empty
                end  
              
              `INSTR_CALL:
                begin
                  $display ("CALL");
                  jsp = jsp + 1;
                end                
               
              default:
                begin
                  $display ("Default case in Phase %d ! \nP_LAT= %b, pattern LD= %b", phase, p_lat, `INSTR_LOAD);
                  $stop();
                end
            endcase
          end
        2:  //Phase 2: exec stage 2
          begin
            p_rd = 0;
            
            casex(p_lat)
              `INSTR_LOAD:
                begin
                  r_wclk = 1;
                end
              
              `INSTR_MOVE:
                begin
                  r_wclk = 1;
                end
                
              `INSTR_RET:
                begin
                  pc = jstack[jsp];
                end
              
              `INSTR_PUSH:
                begin
                  r_wclk = 1;
                  d_out = r_rdat;
                end
              
              `INSTR_POP:
                begin
                  r_wclk = 1;
                end
                
              `INSTR_JIC:
                begin
                    //deliberatery empty
                end
                
              `INSTR_JIZ:
                begin
                    //deliberatery empty
                end  
              
              `INSTR_JMP:
                begin
                    //deliberatery empty
                end  
              
              `INSTR_CALL:
                begin
                  jstack[jsp] = pc;
                end                
              
              default:
                begin
                  $display ("Default case in Phase %d !", phase);
                  $stop();
                end
            endcase
          end
        3:  //Phase 3: exec stage 3
          begin
            casex(p_lat)
              `INSTR_LOAD:
                begin
                  r_wclk  = 0;
                  pc = pc + 1;
                end
                
              `INSTR_MOVE:
                begin
                  r_wclk  = 0;
                  pc = pc + 1;
                end
                
              `INSTR_RET:
                begin
                  jsp = jsp - 1;
                  pc = pc + 1;
                end
              
              `INSTR_PUSH:
                begin
                  pc = pc + 1;
                  d_wr = 1;
                end     
                
              `INSTR_POP:
                begin
                  pc = pc + 1;
                end
              
              `INSTR_JIC:
                begin
                  $display("cflg: %b", r_flg_carry);
                  pc = (r_flg_carry == 1) ? (jpadr) : (pc + 1);
                end
              
              `INSTR_JIZ:
                begin
                  $display("zflg: %b", r_flg_zero);
                  pc = (r_flg_zero == 1) ? jpadr : (pc + 1);
                  
                end
                
              `INSTR_JMP:
                begin
                  pc = jpadr;
                end
                
              `INSTR_CALL:
                begin
                  pc = jpadr;
                end
              
              default:
                begin
                  $display ("Default case in Phase %d !", phase);
                  $stop();
                end
            endcase
          end
      endcase
      
      phase = phase + 1;  //let's not care about overflow~
    end
  end
  
endmodule
