`timescale 1ns / 1ps

/*  Arithmetic/Logic Unit.
 * ----------------------------
 *
 *  General description
 * ----------------------
 *  The ALU is constructed out of %WIDTH% elements, called slices. These slices typically do the actual arithmetics.
 *  There is one more part to the ALU, which is the 'bit shift unit', AKA bit shifter. It is responsible for handling
 *  bit shifts, as the name suggests, and bit rolls as well.
 *
 *  This implementation uses a purely combinatorial design. Implementing a sequential design could be as easy as
 *  adding some latches at the inputs/outputs of the ALU, when desired.
 *
 *  This module also enables the implementer to use the flag outputs, usable for various conditionals, for example.
 *
 *  All operation is controlled by a number of control inputs, described below.
 *
 *
 *  Control inputs
 * -----------------
 *  This module is controlled by several control inputs. Their purpose can be probably best described
 *  by the following ASCII-schematic:
 *
 *
 *                  ---                             ---------------
 *  Input A >------|   |           ------          |               |
 *                 | & |-----------\\  x \      C  |               |
 *          [Az]--o|   |            >> o  >--------|               |<---------- [F]
 *                  ---     [An]---//  r /         |               |
 *                                 -----           |               |          ------
 *                                                 |    F U L L    |----------\\  x \
 *                  ---                            |               |           >> o  >-------> Ouptut (to the bit shifter input)
 *  Input B >------|   |           ------          |               |  [On] ---//  r /
 *                 | & |-----------\\  x \      B  |               |          ------
 *          [Bz]--o|   |            >> o  >--------|               |
 *                  ---     [Bn]---//  r /         |               |
 *                                 -----           |   A D D E R   |-------------------------> Carry to the next stage ('Input C')
 *                                                 |               |
 *                  ---                            |               |
 *  Input C >------|   |                        C  |               |
 *                 | & |---------------------------|               |
 *          [Cz]--o|   |                           |               |
 *                  ---                            |               |
 *                                                  ---------------
 *
 *  Xz : Zeroes the incoming bits for the given input X.
 *  Xn : Negates the incoming bits for the given input X. This operation takes place after the zeroer. 
 *  F  : Selects the output. If F is 0, the output is set to the adder's direct output.
 *                           If F is 1, the output is set to the adder's carry output.
 *                             This can be particularly useful for basically turning the adder into an ANDer,
 *                              but to do that, be sure to turn the Cz on as well. These signals are nott internally connected
 *                              in orer to provide greater flexibility of the design.
 *
 *  On : Ouput negate (before bit shifting)
 *  Cn : Negates the ALU's carry input, not the slice's. Useful for increments/decrements alike, although those can be implemented without it as well.
 * 
 *  Se : Shifter Enable input. 1 to enable shifting, 0 for pass thru.
 *  Sn : Shifter input saying by how much should one be shifting. Wired to Input A.
 *  Sd : Shifter direction.  0 for left shifts, 1 for right shifts.
 *  Sr : Shift/Roll control. 0 to shift, 1 to roll.
 *
 */
 
 module alu(
    ina, inb, cin, fcn,
    cout, out,
    flgzero,flgagtb,flgaeqb
  );

   
  `define BITS_TO_FIT(N) ( \
      N <  2 ? 1 : \
      N <  4 ? 2 : \
      N <  8 ? 3 : \
      N < 16 ? 4 : \
      N < 32 ? 5 : \
      N < 64 ? 6 : 7 )
      
      
  parameter WIDTH = 8;

  input  [WIDTH-1:0] ina;
  input  [WIDTH-1:0] inb;
  
  input  [7:0] fcn;
  
  wire   Az, An, Bz, Bn, Cz, Cn, F, On;
  wire   [`BITS_TO_FIT(WIDTH-1)-1:0] Sn;
  wire   Sd, Sr;
  
  input  cin;
  
  output flgzero;
  output flgagtb;
  output flgaeqb;
  
  output [WIDTH-1:0] out;
  output cout;
  
  wire [WIDTH-1:0]  aluo;
  
  wire [WIDTH:0] carries;       // carry chain itself

  assign carries[0] = cin ^ Cn; // carry input precondition

  /// Flag generation //////////////////////////////////////////////////////////
  
  assign flgaeqb = (ina == inb);
  assign flgagtb = (ina >  inb);
  assign flgzero = (out ==  0 );

  /// ALU slice generator //////////////////////////////////////////////////////
  
  genvar i;

  generate
    for (i=0; i<WIDTH; i=i+1)
    begin:slices
      aluslice _slice (
        .Ain(   ina[i]   ), 
        .Az (     Az     ), 
        .An (     An     ), 
        .Bin(   inb[i]   ), 
        .Bz (     Bz     ), 
        .Bn (     Bn     ), 
        .Cin( carries[i] ), 
        .Cz (     Cz     ),
        .F  (     F      ), 
        .On (     On     ), 
        .Co (carries[i+1]), 
        .O  (  aluo [i]  )
      );
    end
  endgenerate

  assign cout = carries[WIDTH];     // carry output

  
  /// BIT SHIFTER/ROLLER ///////////////////////////////////////////////////////
  
  wire Se;  //shift enable
  
  bitshifter #(.WIDTH(WIDTH)) shifter (
    .d  ( aluo ), 
    .q  ( out  ), 
    .n  ( Se ? Sn : {`BITS_TO_FIT(WIDTH-1){1'b0}} ), 
    .rot( Sr ), 
    .dir( Sd )
  );
  
  
  /// FCN to Control Inputs Look-Up table //////////////////////////////////////
  
  reg [10:0] mode;
  
  assign Sn = ina[3:0];
  assign {Az, An, Bz, Bn, Cz, Cn, F, On, Se, Sr, Sd} = mode;
  
  always @(fcn) begin
    casex( fcn )
                            //-A- -B- -C- F O S S S    // mnem.    output.
                            //z n z n z n f n e r d    // -----------------
      8'bXXXX0000: mode = 11'b0_0_0_0_1_0_1_0_0_0_0 ;  // AND     ( A & B )
      8'bXXXX0001: mode = 11'b0_1_0_1_1_0_1_1_0_0_0 ;  // OR      ( A | B )
      8'bXXXX0010: mode = 11'b0_0_0_0_1_0_0_0_0_0_0 ;  // XOR     ( A ^ B )
      8'bXXXX0011: mode = 11'b0_0_0_1_1_0_0_0_0_0_0 ;  // MASK    ( A &!B )
      8'bXXXX0100: mode = 11'b0_0_0_0_1_0_1_1_0_0_0 ;  // NAND   !( A & B )
      8'bXXXX0101: mode = 11'b0_1_0_1_1_0_1_0_0_0_0 ;  // NOR    !( A | B )
      8'bXXXX0110: mode = 11'b0_0_0_0_1_0_0_1_0_0_0 ;  // NXOR   !( A ^ B )
      8'bXXXX0111: mode = 11'b1_0_1_0_1_0_0_0_0_0_0 ;  // ZERO    (   0   )
      8'bXXXX1000: mode = 11'b0_0_0_0_0_0_0_0_0_0_0 ;  // ADD     ( A + B )
      8'bXXXX1001: mode = 11'b0_1_0_1_0_0_0_1_0_0_0 ;  // SUB     ( A - B )
      8'bXXXX1010: mode = 11'b0_1_1_1_0_0_0_1_0_0_0 ;  // INC     ( A + 1 )
      8'bXXXX1011: mode = 11'b0_0_1_1_0_0_0_0_0_0_0 ;  // DEC     ( A - 1 )
      8'bXXXX1100: mode = 11'b0_0_1_1_1_0_1_0_1_0_0 ;  // SHL     ( B << A)
      8'bXXXX1101: mode = 11'b0_0_1_1_1_0_1_0_1_0_1 ;  // SHR     ( B >> A)
      8'bXXXX1110: mode = 11'b0_0_1_1_1_0_1_0_1_1_0 ;  // ROL     ( B <r A)
      8'bXXXX1111: mode = 11'b0_0_1_1_1_0_1_0_1_1_1 ;  // ROR     ( B >r A)
      default:     mode = 11'b1_0_1_0_1_0_0_0_0_0_0 ;  // default (   0   )
    endcase
  end
    
    
  
endmodule
