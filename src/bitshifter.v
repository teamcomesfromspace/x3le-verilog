`timescale 1ns / 1ps

module bitshifter(d, q, n, rot, dir);

  parameter WIDTH = 16;

  `define BITS_TO_FIT(N) ( \
      N <  2 ? 1 : \
      N <  4 ? 2 : \
      N <  8 ? 3 : \
      N < 16 ? 4 : \
      N < 32 ? 5 : \
      N < 64 ? 6 : 7 )

  parameter NSTAGES = `BITS_TO_FIT(WIDTH-1);

  input  [WIDTH-1:0]   d;
  output [WIDTH-1:0]   q;
  input  [NSTAGES-1:0] n;

  input rot;
  input dir;
  
  wire [WIDTH-1:0] imed_in; 
  wire [WIDTH-1:0] imed_out;
  
  assign imed_out = (imed_in << n) | ((rot) ? (imed_in >> WIDTH-n-1) : 1'b0);
  
  genvar j;
  
  generate
    for(j=0; j<WIDTH; j=j+1)
    begin:dirstage
      assign imed_in[j] = d[(dir) ? (WIDTH-1-j) : j];
      assign q[j]    = imed_out[(dir) ? (WIDTH-1-j) : j];
    end
  endgenerate

endmodule
