`timescale 1ns / 1ps

module x3le_test;

	// Inputs
	reg clk;
	reg reset;
	reg [15:0] p_dat;
	reg [7:0] d_in;

	// Outputs
	wire [11:0] p_adr;
	wire p_rd;
	wire [15:0] d_adr;
	wire [7:0] d_out;
	wire d_wr;
	wire d_rd;

	// Instantiate the Unit Under Test (UUT)
	x3le uut (
		.clk(clk), 
		.reset(reset), 
		.p_dat(p_dat), 
		.p_adr(p_adr), 
		.p_rd(p_rd), 
		.d_adr(d_adr), 
		.d_in(d_in), 
		.d_out(d_out), 
		.d_wr(d_wr), 
		.d_rd(d_rd)
	);

  reg [15:0] pmem [0:255];
  
  reg [ 7:0] stack [0:31];
    
  
	initial
  begin
		// Initialize Inputs
		clk = 0;
		reset = 1;
		p_dat = 0;
		d_in = 0;

		// Wait 100 ns for global reset to finish
		#100;
    reset = 0;
        
    /* //First program to be run there ever
    pmem[ 0] = 16'b00_11001010_001000; //LD r 001000 <- l 11001010
    pmem[ 1] = 16'b1000_001000_001001; //MV r 001001 <- r 001000
    pmem[ 2] = 16'b1010_001001_000000; //PUSH r 000000
    pmem[ 3] = 16'b00_00001111_000000; //LS r 000000 <- l 00001111
    pmem[ 4] = 16'b1010_000000_000000; //PUSH r 000000
    pmem[ 5] = 16'b1011_000000_001010; //POP  r 001010
    pmem[ 6] = 16'b1010_001010_000000; //PUSH r 001010
    pmem[ 7] = 16'b1010_001010_000000; //PUSH r 001010
    
    pmem[ 8] = 16'b00_00001010_000000; //LD r 000000 <- l 00001010 //A = 001010
    pmem[ 9] = 16'b00_00101111_000001; //LD r 000001 <- l 00101111 //B = 101111
    pmem[10] = 16'b00_00001000_000010; //LD r 000010 <- l 00001000 //ADD
    pmem[11] = 16'b1010_000011_000000; //PUSH r 000011             //PUSH result
      
    */

    // Fibonacci number generator - first half useful program running on this thing
    pmem[ 0] = 16'b00_00000001_000000;  //  LD A 1
    pmem[ 1] = 16'b00_00000000_000001;  //  LD B 0
    pmem[ 2] = 16'b00_00001000_000010;  //  LD M ADD
    pmem[ 3] = 16'b1110_000000_000110;  //  JMP ENTR 
                                        //loop:
    pmem[ 4] = 16'b1101_000000_000000;  //  POP A
    pmem[ 5] = 16'b1101_000000_000001;  //  POP B
                                        //entr:
    pmem[ 6] = 16'b1100_000001_000000;  //  PUSH B
    pmem[ 7] = 16'b1100_000000_000000;  //  PUSH A
    pmem[ 8] = 16'b1011_000000_001011;  //  JIC END
    pmem[ 9] = 16'b1100_000011_000000;  //  PUSH O
    pmem[10] = 16'b1110_000000_000100;  //  JMP LOOP
                                        //end:
    pmem[11] = 16'b1110_000000_001011;  //  JMP END
    
    /*
    //CALL and RET test
    
    pmem[ 0] = 16'b1111_000000_000010;  //     CALL r
    pmem[ 1] = 16'b1110_000000_000001;  //end: JMP end
    pmem[ 2] = 16'b1001_000000_000000;  //  r: RET
    */
    
	end
  
  always
    //run clk at 10ns period
    #5  clk = !clk;
    
    
  always @(p_rd) begin
    p_dat = pmem[p_adr[7:0]];
  end
  
  always @(d_wr) begin
    stack[d_adr[5:0]] = d_out;
  end
  
  always @(d_rd) begin
    d_in = stack[d_adr[5:0]];
  end
  
endmodule

