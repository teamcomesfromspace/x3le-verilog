`timescale 1ns / 1ps

module aluslice(
    input   Ain,  //Input A
    input   Az,   //  zero A
    input   An,   //  neg  A
    input   Bin,  //Input B
    input   Bz,   //  zero B
    input   Bn,   //  neg  B
    input   Cin,  //Input C
    input   Cz,   //  zero C
    input   F,    //Funct (Add/And)  //0 = Add, 1 = And
    input   On,   //  neg Out
    
    output  Co,   //Carry Output
    output  O     //Output
  );
  
  wire pA;  //preconditioned A
  wire pB;  //preconditioned B
  wire pC;  //preconditioned C
  
  assign pA = (Ain & !Az) ^ An; //
  assign pB = (Bin & !Bz) ^ Bn; //
  assign pC = (Cin & !Cz);      //
  
  wire [1:0] iRes;  //intermediate result
  
  assign iRes = {1'b0, pA} + {1'b0, pB} + {1'b0, pC};
  
  assign Co = iRes[1];  //carry out
  assign O = (iRes[F] ^ On);  //output

endmodule
